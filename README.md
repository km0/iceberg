# Iceberg

![Three different iceberg aha](cover.jpg)

This repo contains some resources to kickstart a series of _comparative iceberg studies_. 

Using the iceberg meme template as a device to investigate & navigate through different topics, the comparative iceberg studies question the organisation of knowledge typical of closed internet communities, as well as the topology of their niches.

Ahahah ooooook. Moore concretely: as first tryout, the plan is to collect several iceberg memes about various topics and curate them together, to see what do they say about each other.

Imagine a small booklet with a collection of 32 iceberg memes and a short essay to contextualize the _comparative iceberg studies_ (maybe iceberg shaped 👀 )


## iceberg.py

Currently a simple scraper to get iceberg memes from DuckDuckGo images. 

```
# Create a virtual environment
python3 -m venv venv

# Install the dependencies
pip install -e .

# Run the script
python iceberg.py
```

It will save images in the `img` folder. 

