from selenium import webdriver
from selenium.webdriver.common.by import By
import urllib.request
import ssl
import os

# Set unverified context to prevent [SSL: CERTIFICATE_VERIFY_FAILED] error
ssl._create_default_https_context = ssl._create_unverified_context


def duck_url(query):
    q = query.replace(' ', '+')
    return f'https://duckduckgo.com/?va=n&hps=1&q={q}&atb=v366-1&iax=images&ia=images'


query = 'iceberg meme'

driver = webdriver.Chrome()
driver.implicitly_wait(2)

driver.get(duck_url(query))

print(driver.title)
images = driver.find_elements(By.CSS_SELECTOR, ".tile-wrap img")
print(f'{len(images)} total images')


for img in images:
    url = img.get_attribute('src')
    urllib.request.urlretrieve(url, os.path.join('img', os.path.basename(url)))
    print(f'Saved image: {img.get_attribute("alt")}')
